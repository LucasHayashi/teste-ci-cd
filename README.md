## CI/CD usado FTP

## Explicação

Quando um commit é realizado para uma branch diferente da padrão (main), é realizado um deploy automático no servidor de homologação, salvando todo o conteúdo da branch da feature no servidor, exceto dos arquivos .git para não ficar pesado.

Após a merge request, irá iniciar um pipe na branch padrão (main) fazendo o mesmo processo, só que dessa vez enviando para o servidor de produção.

## Servidores de teste utilizados

[Homologação](http://testecicd.ezyro.com/)
[Produção](http://testecicdmerged.ezyro.com)

## Referências utilizadas para o desenvolvimento

[GitLab CI: Upload files with FTP](https://medium.com/devops-with-valentine/gitlab-ci-upload-files-with-ftp-dee357ceecce)

[How to deploy any project using FTP with Gitlab Continous Integration (CI)?](https://mrkaluzny.com/blog/how-to-deploy-any-project-using-ftp-with-gitlab-continous-integration-ci/)

[GitLab CI: Deploy to FTP / SFTP with lftp](https://savjee.be/blog/gitlab-ci-deploy-to-ftp-with-lftp/)

[GitLab CI: Deploy to FTP / SFTP with lftp](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

[Criar Regras](https://docs.gitlab.com/ee/ci/yaml/#rules)